import { parseConfigFileTextToJson, parseJsonConfigFileContent } from "typescript";

let attempts = 3;

export function load({ cookies }) {
	let random_number = Math.floor(Math.random() * 100);
	let cooki = cookies.get('zagadka');
	let cookiobj = { randnum: random_number, attempt: 0 }
	if (!cooki) {
		cookies.set('zagadka', JSON.stringify(cookiobj))

	}

	return {
		toShow: cookies.get('toshow')
	}
}

export const actions = {
	check: async ({ cookies, request }) => {
		const data = await request.formData();

		let cooki = (cookies.get('zagadka'));
		let parscoo = JSON.parse(cooki)
		let datav = data.get('num')
		let rn = parscoo.randnum
		let at = parscoo.attempt
		console.log(parscoo.randnum)
		console.log(parscoo.attempt)
		console.log(datav)

		attempts--;

		if(attempts == 0) {
			cookies.set('at', attempts - at);
			return;
	   }
		if (datav == parscoo.randnum) {
			cookies.set('toshow', 'Вы угадали!');
		} else if (datav > 100 || datav < 0) {
			cookies.set('toshow', 'Введите число от 0 до 100. Осталось попыток:' + (attempts - at  ));
		} else if (datav > rn) {
			cookies.set('toshow', 'Вы ввели число больше! Осталось попыток:' + (attempts - at ));
		} else if (datav < rn) {
			cookies.set('toshow', 'Вы ввели число меньше!Осталось попыток:' + (attempts - at ));
		}
	}
};

